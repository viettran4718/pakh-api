package com.vhc.pakh.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DataSourceConfig {
	@Value("${spring.datasource.url}")
	String url;
	@Value("${spring.datasource.username}")
	String username;
	@Value("${spring.datasource.password}")
	String password;


	@Bean
	public DataSource dataSource() {
		return DataSourceBuilder.create()
				.url(url)
				.driverClassName("oracle.jdbc.OracleDriver")
				.username(username)
				.password(password).build();
	}
}

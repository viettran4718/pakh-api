package com.vhc.pakh.entities;

//import com.hq.mght.dal.TdhoadonDAO;
//import com.xdev.dal.DAO;
//import com.xdev.util.Caption;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * Tdhoadon
 */
//@DAO(daoClass = TdhoadonDAO.class)
//@Caption("{%sohochieukhachhang}")
@Entity
@Table(name = "TDHOADON", schema = "MGH_USER")
public class Tdhoadon implements java.io.Serializable {

	private long tdhoadonId;
	private Short loaihinhnhapcanhKhId;
	private String sohochieukhachhang;
	private String hovatenkhachhang;
	private String quoctichkhachhang;
	private Long tshoadontrangthaiId;
	private String mauso;
	private String kyhieu;
	private Long so;
	private String quyen;
	private Date ngaylap;
	private String hinhthucthanhtoan;
	private String sotaikhoan;
	private String nganhang;
	private String swiftcode;
	private String image;
	private Date ngaybanhang;
	private String nguoitao;
	private Date ngaytao;
	private Double tongtienhang;
	private Double thuesuat;
	private Double tienthue;
	private Double tongthanhtoan;
	private String nguoibanhang;
	private String nguoimuahang;
	private Date ngaysuacuoi;
	private String nguoisuacuoi;
	private String nguoidaidiencuahang;
//	private Set<Tdhoadonchitiet> tdhoadonchitiets = new HashSet<Tdhoadonchitiet>(0);
//	private Set<Xulyhosovat> xulyhosovats = new HashSet<Xulyhosovat>(0);

	public Tdhoadon() {
	}

	//@Caption("TdhoadonId")
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Id

	@Column(name = "TDHOADON_ID", unique = true, nullable = false, columnDefinition = "NUMBER", precision = 18, scale = 0)
	public long getTdhoadonId() {
		return this.tdhoadonId;
	}

	public void setTdhoadonId(long tdhoadonId) {
		this.tdhoadonId = tdhoadonId;
	}



	//@Caption("LoaihinhnhapcanhKhId")
	@Column(name = "LOAIHINHNHAPCANH_KH_ID", columnDefinition = "NUMBER", precision = 3, scale = 0)
	public Short getLoaihinhnhapcanhKhId() {
		return this.loaihinhnhapcanhKhId;
	}

	public void setLoaihinhnhapcanhKhId(Short loaihinhnhapcanhKhId) {
		this.loaihinhnhapcanhKhId = loaihinhnhapcanhKhId;
	}

	//@Caption("Sohochieukhachhang")
	@Column(name = "SOHOCHIEUKHACHHANG", columnDefinition = "NVARCHAR2", length = 100)
	public String getSohochieukhachhang() {
		return this.sohochieukhachhang;
	}

	public void setSohochieukhachhang(String sohochieukhachhang) {
		this.sohochieukhachhang = sohochieukhachhang;
	}

	//@Caption("Hovatenkhachhang")
	@Column(name = "HOVATENKHACHHANG", columnDefinition = "NVARCHAR2", length = 200)
	public String getHovatenkhachhang() {
		return this.hovatenkhachhang;
	}

	public void setHovatenkhachhang(String hovatenkhachhang) {
		this.hovatenkhachhang = hovatenkhachhang;
	}

	//@Caption("Quoctichkhachhang")
	@Column(name = "QUOCTICHKHACHHANG", columnDefinition = "NVARCHAR2", length = 100)
	public String getQuoctichkhachhang() {
		return this.quoctichkhachhang;
	}

	public void setQuoctichkhachhang(String quoctichkhachhang) {
		this.quoctichkhachhang = quoctichkhachhang;
	}

	//@Caption("TshoadontrangthaiId")
	@Column(name = "TSHOADONTRANGTHAI_ID", columnDefinition = "NUMBER", precision = 2, scale = 0)
	public Long getTshoadontrangthaiId() {
		return this.tshoadontrangthaiId;
	}

	public void setTshoadontrangthaiId(Long tshoadontrangthaiId) {
		this.tshoadontrangthaiId = tshoadontrangthaiId;
	}

	//@Caption("Mauso")
	@Column(name = "MAUSO", columnDefinition = "NVARCHAR2", length = 100)
	public String getMauso() {
		return this.mauso;
	}

	public void setMauso(String mauso) {
		this.mauso = mauso;
	}

	//@Caption("Kyhieu")
	@Column(name = "KYHIEU", columnDefinition = "NVARCHAR2", length = 100)
	public String getKyhieu() {
		return this.kyhieu;
	}

	public void setKyhieu(String kyhieu) {
		this.kyhieu = kyhieu;
	}

	//@Caption("So")
	@Column(name = "SO", columnDefinition = "NUMBER", precision = 10, scale = 0)
	public Long getSo() {
		return this.so;
	}

	public void setSo(Long so) {
		this.so = so;
	}

	//@Caption("Quyen")
	@Column(name = "QUYEN", columnDefinition = "NVARCHAR2", length = 100)
	public String getQuyen() {
		return this.quyen;
	}

	public void setQuyen(String quyen) {
		this.quyen = quyen;
	}

	//@Caption("Ngaylap")
	@Temporal(TemporalType.DATE)
	@Column(name = "NGAYLAP", columnDefinition = "DATE", length = 7)
	public Date getNgaylap() {
		return this.ngaylap;
	}

	public void setNgaylap(Date ngaylap) {
		this.ngaylap = ngaylap;
	}

	//@Caption("Hinhthucthanhtoan")
	@Column(name = "HINHTHUCTHANHTOAN", columnDefinition = "NVARCHAR2", length = 100)
	public String getHinhthucthanhtoan() {
		return this.hinhthucthanhtoan;
	}

	public void setHinhthucthanhtoan(String hinhthucthanhtoan) {
		this.hinhthucthanhtoan = hinhthucthanhtoan;
	}

	//@Caption("Sotaikhoan")
	@Column(name = "SOTAIKHOAN", columnDefinition = "NVARCHAR2", length = 100)
	public String getSotaikhoan() {
		return this.sotaikhoan;
	}

	public void setSotaikhoan(String sotaikhoan) {
		this.sotaikhoan = sotaikhoan;
	}

	//@Caption("Nganhang")
	@Column(name = "NGANHANG", columnDefinition = "NVARCHAR2", length = 100)
	public String getNganhang() {
		return this.nganhang;
	}

	public void setNganhang(String nganhang) {
		this.nganhang = nganhang;
	}

	//@Caption("Swiftcode")
	@Column(name = "SWIFTCODE", columnDefinition = "NVARCHAR2", length = 100)
	public String getSwiftcode() {
		return this.swiftcode;
	}

	public void setSwiftcode(String swiftcode) {
		this.swiftcode = swiftcode;
	}

	//@Caption("Image")
	@Column(name = "IMAGE", columnDefinition = "NVARCHAR2", length = 100)
	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	//@Caption("Ngaybanhang")
	@Temporal(TemporalType.DATE)
	@Column(name = "NGAYBANHANG", columnDefinition = "DATE", length = 7)
	public Date getNgaybanhang() {
		return this.ngaybanhang;
	}

	public void setNgaybanhang(Date ngaybanhang) {
		this.ngaybanhang = ngaybanhang;
	}

	//@Caption("Nguoitao")
	@Column(name = "NGUOITAO", columnDefinition = "NVARCHAR2", length = 200)
	public String getNguoitao() {
		return this.nguoitao;
	}

	public void setNguoitao(String nguoitao) {
		this.nguoitao = nguoitao;
	}

	//@Caption("Ngaytao")
	@Temporal(TemporalType.DATE)
	@Column(name = "NGAYTAO", columnDefinition = "DATE", length = 7)
	public Date getNgaytao() {
		return this.ngaytao;
	}

	public void setNgaytao(Date ngaytao) {
		this.ngaytao = ngaytao;
	}

	//@Caption("Tongtienhang")
	@Column(name = "TONGTIENHANG", columnDefinition = "NUMBER", precision = 18)
	public Double getTongtienhang() {
		return this.tongtienhang;
	}

	public void setTongtienhang(Double tongtienhang) {
		this.tongtienhang = tongtienhang;
	}

	//@Caption("Thuesuat")
	@Column(name = "THUESUAT", columnDefinition = "NUMBER", precision = 4)
	public Double getThuesuat() {
		return this.thuesuat;
	}

	public void setThuesuat(Double thuesuat) {
		this.thuesuat = thuesuat;
	}

	//@Caption("Tienthue")
	@Column(name = "TIENTHUE", columnDefinition = "NUMBER", precision = 18, scale = 5)
	public Double getTienthue() {
		return this.tienthue;
	}

	public void setTienthue(Double tienthue) {
		this.tienthue = tienthue;
	}

	//@Caption("Tongthanhtoan")
	@Column(name = "TONGTHANHTOAN", columnDefinition = "NUMBER", precision = 18)
	public Double getTongthanhtoan() {
		return this.tongthanhtoan;
	}

	public void setTongthanhtoan(Double tongthanhtoan) {
		this.tongthanhtoan = tongthanhtoan;
	}

	//@Caption("Nguoibanhang")
	@Column(name = "NGUOIBANHANG", columnDefinition = "NVARCHAR2", length = 100)
	public String getNguoibanhang() {
		return this.nguoibanhang;
	}

	public void setNguoibanhang(String nguoibanhang) {
		this.nguoibanhang = nguoibanhang;
	}

	//@Caption("Nguoimuahang")
	@Column(name = "NGUOIMUAHANG", columnDefinition = "NVARCHAR2", length = 100)
	public String getNguoimuahang() {
		return this.nguoimuahang;
	}

	public void setNguoimuahang(String nguoimuahang) {
		this.nguoimuahang = nguoimuahang;
	}

	//@Caption("Ngaysuacuoi")
	@Temporal(TemporalType.DATE)
	@Column(name = "NGAYSUACUOI", columnDefinition = "DATE", length = 7)
	public Date getNgaysuacuoi() {
		return this.ngaysuacuoi;
	}

	public void setNgaysuacuoi(Date ngaysuacuoi) {
		this.ngaysuacuoi = ngaysuacuoi;
	}

	//@Caption("Nguoisuacuoi")
	@Column(name = "NGUOISUACUOI", columnDefinition = "NVARCHAR2", length = 40)
	public String getNguoisuacuoi() {
		return this.nguoisuacuoi;
	}

	public void setNguoisuacuoi(String nguoisuacuoi) {
		this.nguoisuacuoi = nguoisuacuoi;
	}

	//@Caption("Nguoidaidiencuahang")
	@Column(name = "NGUOIDAIDIENCUAHANG", columnDefinition = "NVARCHAR2", length = 40)
	public String getNguoidaidiencuahang() {
		return this.nguoidaidiencuahang;
	}

	public void setNguoidaidiencuahang(String nguoidaidiencuahang) {
		this.nguoidaidiencuahang = nguoidaidiencuahang;
	}

	//@Caption("Tdhoadonchitiets")
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tdhoadon")
//	public Set<Tdhoadonchitiet> getTdhoadonchitiets() {
//		return this.tdhoadonchitiets;
//	}
//
//	public void setTdhoadonchitiets(Set<Tdhoadonchitiet> tdhoadonchitiets) {
//		this.tdhoadonchitiets = tdhoadonchitiets;
//	}
//
//	//@Caption("Xulyhosovats")
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tdhoadon")
//	public Set<Xulyhosovat> getXulyhosovats() {
//		return this.xulyhosovats;
//	}
//
//	public void setXulyhosovats(Set<Xulyhosovat> xulyhosovats) {
//		this.xulyhosovats = xulyhosovats;
//	}

}

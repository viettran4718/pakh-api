package com.vhc.pakh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.vhc.pakh.entities.Tdhoadon;


public interface TdhoadonRepository extends JpaRepository<Tdhoadon,Integer> {
	void deleteByTdhoadonId(long id);
	Tdhoadon findByTdhoadonId(long id);
}

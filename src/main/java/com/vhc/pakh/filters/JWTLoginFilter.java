package com.vhc.pakh.filters;


import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.vhc.pakh.model.AccountCredentials;
import com.vhc.pakh.service.TokenAuthenticationService;

/**
 * Created by vietdz on 18/07/2019.
 */
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
	
	public String generateMD5(String initString) throws NoSuchAlgorithmException {	
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(initString.getBytes());
		byte byteData[] = md.digest();
		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		
		return sb.toString();
	}
	
    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        AccountCredentials credentials;
		credentials = new AccountCredentials(request.getHeader("username"), request.getHeader("password"));
		try {
			return getAuthenticationManager().authenticate(
			        new UsernamePasswordAuthenticationToken(
			                credentials.getUsername(),
			                generateMD5(credentials.getPassword()),
			                Collections.emptyList()
			        )
			);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        TokenAuthenticationService.addAuthentication(response, authResult.getName());
    }
}

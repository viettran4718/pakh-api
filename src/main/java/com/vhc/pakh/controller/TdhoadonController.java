package com.vhc.pakh.controller;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vhc.pakh.entities.Tdhoadon;
import com.vhc.pakh.service.TdhoadonService;

/**
 * Created by vietdz on 18/07/2019.
 */
@RestController
@RequestMapping(value = "/tdhoadon")
public class TdhoadonController {

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private TdhoadonService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Tdhoadon> getReportInfo() throws NoSuchAlgorithmException, SQLException {
		return service.findAll();
	}

}

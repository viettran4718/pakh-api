package com.vhc.pakh.service;

import java.util.List;

import com.vhc.pakh.entities.Tdhoadon;

public interface TdhoadonService {
	List<Tdhoadon> findAll();

	List<Tdhoadon> search(String q);

	Tdhoadon findOne(long id);

	void save(Tdhoadon object);

	void delete(long id);

}

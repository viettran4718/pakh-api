package com.vhc.pakh.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vhc.pakh.entities.Tdhoadon;
import com.vhc.pakh.repository.TdhoadonRepository;
import com.vhc.pakh.service.TdhoadonService;

@Service
public class TdhoadonServiceImpl implements TdhoadonService {

	@Autowired
	TdhoadonRepository repository;

	@Override
	public List<Tdhoadon> findAll() {
		// TODO Auto-generated method stub
		return (List<Tdhoadon>) repository.findAll();
	}

	@Override
	public List<Tdhoadon> search(String q) {
		// TODO Auto-generated method stub
		return (List<Tdhoadon>) repository.findAll();
	}

	@Override
	public Tdhoadon findOne(long id) {
		// TODO Auto-generated method stub
		return repository.findByTdhoadonId(id);
	}

	@Override
	public void save(Tdhoadon object) {
		// TODO Auto-generated method stub
		repository.save(object);
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		repository.delete(repository.findByTdhoadonId(id));
	}

}

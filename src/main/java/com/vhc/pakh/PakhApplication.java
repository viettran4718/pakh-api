package com.vhc.pakh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PakhApplication {

	public static void main(String[] args) {
		SpringApplication.run(PakhApplication.class, args);
	}

}
